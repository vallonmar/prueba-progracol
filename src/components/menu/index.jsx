import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Row, Col } from 'react-flexbox-grid';
import '../../styles/main.sass';

class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active : false
        }
    }
    icon= ( name ) => {
        let icon;
        switch (name) {
            case 'clientes':
                return icon = <img src='../../images/iconos/users.svg' alt=''/>
            default:
                break;
        } 
    }
    logout = () => {
        localStorage.clear();
    }
    active = (active) =>{
        this.setState({ active : !active })
    }
    render (){
        const { active } = this.state
        const props = this.props;
        
        return (
            <div className={'menu_active'} onClick={() => this.active(active)}>
                <div className="logo">
                    <img src="../../images/iconos/iconologo.svg" alt=""/>
                </div>
                {
                    props.views.map((view, index) => {
                        let ruta = `/usuario/${props.name}${view.path}`;
                        return <Link
                            key = {index}
                            className = {props.tab === view.name.toLowerCase() ? 'activa': ''} 
                            name = {view.name.toLowerCase()} 
                            to = {ruta} 
                            onClick = {(e) => props.changeTab(e, view.name.toLowerCase() )}
                        > 
                        <div className='icon'>
                            {this.icon(view.name)}
                            <span className='text'>{view.name}</span>
                        </div>
                        </Link>
                    })
                }
            </div>
        )
    }
}
export default Menu;