import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Row, Col } from 'react-flexbox-grid';
import '../../styles/main.sass';

class Header extends Component {
    constructor() {
        super();
        this.state = {
            login: false
        }
    }
    logout = () => {
        localStorage.clear();
        this.setState({ login : true })
    }
    render (){
        const { login } = this.state
        if ( login ){
            return <Redirect to={`/`} />;
        }
        return (
            <div className='header'>
                <Row className='noMargin'>
                    <Col xsOffset={11} xs={1}>
                        <p className='logout' onClick={this.logout}>Salir</p>
                        <div className='logo'>
                            <img src='../../images/iconos/user.svg' alt='' width='100%'/>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}
export default Header;