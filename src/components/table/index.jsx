import React, { Component } from 'react';
import ReactTable from 'react-table';
import "react-table/react-table.css";
import '../../styles/main.sass'; 
import Pagination from "./pagination.jsx";

class Table extends Component {
    constructor(props){
        super(props);
        this.state = {
            columns: [],
            data: {},
            total: 8
        }
    }
    componentWillReceiveProps = (next) => {
        this.setState({...next})
    }
    componentDidMount = () =>{
        this.setState({...this.props})
    }
    componentWillMount = () =>{
        this.setState({...this.props})
    }
    
render() {
    const { data, columns, total  } = this.state;
    
    return (
        <div>
            <ReactTable
                data={data}
                columns={columns}
                className="-striped -highlight"
                noDataText= {false}
                pages={Math.floor(total/9) + 1}
                pageSize={8}
                filterable
                PaginationComponent={Pagination}
                loadingText = {'Cargando...'}
                defaultSorted={[{ id: "createdAt", desc: true }]}
                getTdProps={() => {
                    return {
                        style: {
                            display: 'flex',
                            flexDirection: 'column',
                            justifyContent: 'center',
                        }
                    };
                }}
                getTrProps={() => {
                    return {
                        style: {
                        textAlign: 'center',
                        }
                    };
                    }}
                getTheadTrProps={() => {
                return {
                    style: {
                        color: 'white',
                        borderTopLeftRadius:'0.8rem',
                        borderTopRightRadius:'0.8rem',
                        backgroundColor: '#3f2d96',
                    }
                };
                }}
                getTheadThProps={() => {
                    return {
                        style: {
                            borderRight: '1px solid white',
                            backgroundColor: '#3f2d96',
                        }
                    };
                }}
            /> 
        </div>
    );
  }
  
}

export default Table;
