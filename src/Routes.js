import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from "react-router-dom"
import Login from './containers/login'
import Main from './containers/main'

const Routes = () => (
    <Router>
        <Switch>
            <Route path="/" exact component={props => <Login {...props} />} />
            <Route path='/usuario/:name/' component={Main} />
            <Route path='/usuario/:name/:path' component={Main} />
        </Switch>
    </Router>
);
export default Routes;