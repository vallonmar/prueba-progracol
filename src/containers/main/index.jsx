import React, { Component } from 'react';
import { Row, Col } from 'react-flexbox-grid';
import { Redirect } from 'react-router-dom';
import Clientes from '../clients';
import NuevoCliente from '../clients/new'
import Header from '../../components/header';
import Menu from '../../components/menu';
import '../../styles/main.sass';

class Main extends Component {
    constructor() {
        super();
        this.state = {
            login : false,
            token : localStorage.getItem('token'),
            name : localStorage.getItem('user'),
            active : false, 
            tab: "clientes",
            tabs: [],
            views: [],
            content: [],
        }
    }
    componentDidMount = () => {
        const { token } = this.state;
        if (token === null){
            this.setState({ login : true })
        } else {
            this.getViews();
            const pathCurrent = this.props.history.location.pathname.split('/');
            if ( pathCurrent[3] !== 'clientes' ){
                this.setState({ tab : 'nuevocliente' })
            }
        } 
    }
    getViews = () => {
        this.setState({
            views : [
                {name: "clientes", path: "/clientes"},
            ]
        })
    };
    changeTab = (e, nameR) => {
        const name  = e.target.name;
        const ruta = nameR !== 'clientes' ? 'nuevocliente' : 'clientes'
        this.setState({tab: ruta});  
        if ( nameR !== 'clientes'){
            this.props.history.push(`/usuario/${this.state.name}/${nameR}`)
        }
    };
    children = (tab, changeTab) => {  
        const content = {
            "clientes": <Clientes changeTab = {changeTab}/>,
            "nuevocliente": <NuevoCliente props = {this.props}/>,
        };
        return content[tab];
    };

    closeModalErrors = () => {
        this.setState({ modalErrors : false })
    }
    render (){
        const { login, tab, views, name } = this.state
        if ( login ){
            return <Redirect to={`/`} />;
        }

        return (
            <div>
                <Row className='noMargin'>
                    <Col xs={1} className='noMargin'>
                        <Menu changeTab={this.changeTab} tab={tab} views={views} name={name}/>
                    </Col>
                    <Col xs={11} className='noMargin'>
                        <Header/>
                        { this.children( tab, this.changeTab )}
                    </Col>
                </Row>
            </div>
        )
    }
}
export default Main;