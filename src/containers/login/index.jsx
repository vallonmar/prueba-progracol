import React, { Component } from 'react';
import axios from 'axios';
import swal from 'sweetalert2';
import { Redirect } from 'react-router-dom';
import '../../styles/main.sass';

class Login extends Component {
    constructor() {
        super();
        this.state = {
            username : '',
            password : '',
            modalErrors : false,
            error : 0,
            disabled : false,
            main :  false,
            token: ''
        }
    }
    componentDidMount = () => {
        const data  = {
            grant_type: 'client_credentials',
            client_id: 4,
            client_secret: 'geUpKNu1eH1XxvwFpiccS9mOocYKNJF4Iz9rg9cS',
            scope: ''
        }
        axios.post('oauth/token', data )
        .then( res => {
            const token = res.token_type.concat(" ",res.access_token)
            this.setState({ token })
            this.setState({ main : true })
        }).catch(error => {
            swal({
                type: 'error',
                title: error.request.status,
                text: error.message
            })
        })
    }
    handleChange = (e) => {
        let name= e.target.name;
        this.setState({[name]:e.target.value}, this.valid());
    }
    valid = () => {
        const { username, password } = this.state 
        const validUser = username.length > 2 
        const validPassword = password.length > 2 
        
        const valid = ( validUser && validPassword)
        this.setState({ disabled : valid })
    }
    handleSumit = (e) =>{
        e.preventDefault();
        const {username, password, token } = this.state
        if ( username && password ) {
            const data = {
                username : username,
                password : password
            }
            const headers = { 'authorization': `Bearer ${token}` };
            axios.post('auth/login/v1/', data, { headers: headers })
            .then( res => {
                localStorage.setItem('accessToken', res.accessToken)
                localStorage.setItem('user', username)
                this.setState({ main : true })
            }).catch(error => {
                swal({
                    type: 'error',
                    title: error.request.status,
                    text: error.message
                })
            })
        } else {
            swal({
                type: 'warning',
                title: 'Lo sentimos',
                text: 'todos los campos son obligatorios'
            })
        } 
    }
    render (){
        const { main, disabled, username} = this.state
        
        if ( main ){
            return <Redirect to={`/usuario/${username}/clientes`} />;
        }
        return (
            <div className='login'>
                <div className='form'>
                    <div className='logo'><img src='../../images/logo/logo.svg' alt='prueba' /></div>
                    <p className='input'>
                        <input name={'username'} onChange={this.handleChange} type="text" placeholder="Usuario" required />
                        <span className="icon"><i className="fas fa-lg fa-user-circle"></i></span>
                    </p>
                    <p className='input'>
                        <input name={'password'} onChange={this.handleChange} type="password" placeholder="Contraseña" required />
                        <span className="icon"><i className="fas fa-lg fa-user-circle"></i></span>
                    </p>
                    <button type="submit" className={disabled ? 'button_active' : 'button'} onClick={this.handleSumit}>Ingresar</button>
                </div>
            </div>
        )
    }
}
export default Login;