import React, { Component } from 'react';
import axios from 'axios';
import swal from 'sweetalert2';
import Table from '../../components/table';
import '../../styles/main.sass';

class Clients extends Component {
    constructor() {
        super();
        this.state = {
            clients: [],
            token : localStorage.getItem('token'),
        }
    }
    componentDidMount = () => {
        const { token } = this.state
        const headers = { 'authorization': `Bearer ${token}` };
        axios.get('v1/clients', { headers: headers })
        .then( res => {
            this.setState({ clients : res.data })
        }).catch(error => {
            swal({
                type: 'error',
                title: error.request.status,
                text: error.message
            })
        })
    }
    new = (e) => {
        this.props.changeTab(e, 'nuevocliente')
    }
    detail = (e, client) => {
        this.props.changeTab(e, client.id)
    }
    render (){
        const {clients} = this.state
        let columns = [{
            Header: 'Name',
            accessor: 'name',
        },{
            Header: 'Sector',
            accessor: 'sector_id',
        },{
            Header: 'Full Time',
            accessor: 'FullTime',
            filterable: false,

            Cell: props => {
                return (
                    <div>
                        <label class="container">
                            <input type="checkbox" />
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    );
                },
        },{
            Header: 'Manager',
            id: "Manager",
            filterable: false,
            Cell: props => {
            return (
                <div>
                    <label class="radioBotton">
                        <input type="radio" name="radio"/>
                        <span class="radiocheck"></span>
                    </label>
                </div>
                );
            }
        },{
                Header: 'Detail',
                id: "Manager",
                filterable: false,
                Cell: props => {
                return (
                    <div>
                        <div className='imgDetail' onClick={(e) => this.detail(e, props.original)} ><img src='../../../images/iconos/plus.svg' width='70%' title='Ver detalles' alt='' /></div>
                    </div>
                    );
                },
        }]; 
        return (
            <div className='client'>
                <h2 className='title'>List Clients</h2>
                <button className='buttonNew' onClick={this.new}>New</button>
                { clients.length > 0 ?
                    <Table
                        columns= {columns}
                        data = {clients}
                    /> : <div className='notData'><img src='../../images/iconos/database.svg'/><br/>Not Data ...</div>
                }
            </div>
        )
    }
}
export default Clients;