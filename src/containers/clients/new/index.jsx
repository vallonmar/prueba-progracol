import React, { Component } from 'react';
import { Row, Col } from 'react-flexbox-grid';
import axios from 'axios';
import swal from 'sweetalert2';
import '../../../styles/main.sass';

class NewClient extends Component {
    constructor() {
        super();
        this.state = {
            edit: false,
            create: false,
            view: true,
            add: false,
            token : localStorage.getItem('token'),        
            name: '',
            email: '',
            address: '',
            sector_id: 0,
            contacts: [],
            nameContac: '',
            emailContact : ''
        }
    }
    handleChange = (e) => {
        let name= e.target.name;
        this.setState({[name]:e.target.value});
    }
    componentDidMount = () => {
        const pathCurrent = this.props.props.history.location.pathname.split('/');
        const { token } = this.state
        if ( pathCurrent[3] === 'nuevocliente' ){
            this.setState({ create: true, edit: false, view: false })
        } else {
            this.setState({ create: false, edit: false, view: true })
            const headers = { 'authorization': `Bearer ${token}` };
            axios.get(`v1/clients/${pathCurrent[3]}`, { headers: headers })
            .then( res => {
                this.setState({ ...res.data })
            }).catch(error => {
                swal({
                    type: 'error',
                    title: error.request.status,
                    text: error.message
                })
            })
        }
    }
    dalete = () => {
        const pathCurrent = this.props.props.history.location.pathname.split('/');
        const { token } = this.state
        const headers = { 'authorization': `Bearer ${token}` };
        axios.delete(`v1/clients/${pathCurrent[3]}`, { headers: headers })
        .then( res => {
            swal({
                type: 'success',
                text: res.message
            })
        }).catch(error => {
            swal({
                type: 'error',
                title: error.request.status,
                text: error.message
            })
        })
    }
    update = () =>{
        this.setState({ view: false, edit: true})
    }
    add = () => {
        this.setState({ add : true})
    }
    saveContact = () => {
        const { nameContac, emailContact, contacts } = this.state
        if ( nameContac === '', emailContact === ''){
            swal({
                type: 'warning',
                text: 'el nombre y el correo del contacto son obligatorios'
            })
        } else {
            contacts.push({ name : nameContac,  email : emailContact})
            this.setState({ nameContac : '' , emailContact : '' , add : false , contacts: contacts})
        }
    }
    save = (e) => {
        e.preventDefault();
        const { create, token, name, email, address,  sector_id, contacts } = this.state
        const pathCurrent = this.props.props.history.location.pathname.split('/');
        const headers = { 'authorization': `Bearer ${token}` };
        const data = {
            name: name,
            email: email,
            address: address,
            sector_id: sector_id,
            contacts: contacts
        }
        axios.put(`v1/clients/${pathCurrent[3]}`, data,  { headers: headers })
        .then( res => {
            swal({
                type: 'success',
                text: res.message
            })
        }).catch(error => {
            swal({
                type: 'error',
                title: error.request.status,
                text: error.message
            })
        })
        if ( create === false ){
            this.setState({ add : false, edit : false, view : true })
        } else { this.setState({ name : '', email : '', address : '', sector_id : '', contacts : [] }) }
    }
    render (){
        const { edit, create, view, name, email, address, sector_id, contacts, add } = this.state
        
        return (
            <div className='detailCLient'>
                <div className='title'>{ create ?  '  Client  Create' : ' Client  Detail' }</div>
                <Row className='noMargin'>
                    <Col xs={5}>
                        <div className='info'>
                            <h3 className='subtitle'>INFO</h3>
                            <div className='data'>
                                {view ? 
                                    <React.Fragment>
                                        <p><b className='b'>Name :</b> { name }</p>
                                        <p><b className='b'>Email  :</b> { email }</p>
                                        <p><b className='b'>Address :</b> { address }</p>
                                        <p><b className='b'>Sector :</b> { sector_id }</p>
                                    </React.Fragment>
                                    : 
                                    <React.Fragment>
                                        <b className='b'>Name :</b>
                                        <input name={'name'} onChange={this.handleChange} value={name} type="text" placeholder="Name" required />
                                        <b className='b'>Email :</b>
                                        <input name={'email'} onChange={this.handleChange} value={email} type="text" placeholder="Email" required />
                                        <b className='b'>Address :</b>
                                        <input name={'address'} onChange={this.handleChange} value={address} type="text" placeholder="Address" required />
                                        <b className='b'>Sector :</b>
                                        <input name={'sector_id'} onChange={this.handleChange} value={sector_id} type="text" placeholder="Sector" required />
                                    </React.Fragment>
                                }
                            </div>
                        </div>
                        
                    </Col>
                    <Col xs={7}>
                        <div className='contac'>
                            <h3 className='subtitle'>CONTACTS</h3>
                            <div className='data'>
                                <React.Fragment>
                                    {contacts.length > 0 ? 
                                        contacts.map(( contact, key) => {
                                        return <Row key={key} className='contacts'>
                                            <Col xs={5}><b className='b'>Name :</b> { contact.name }</Col>
                                            <Col xs={7}><b className='b'>Email :</b> { contact.email }</Col>
                                        </Row>
                                    })
                                    : <div className='noContact'><img src='../../images/iconos/address.svg'/><br/>Not Contact ...</div>
                                    }
                                    {create || edit ?
                                        <Row>
                                            <Col xs={12}><button className='add' onClick={this.add}>Add Contact</button>
                                            {add ?
                                                <button className='addContact' onClick={this.saveContact}>Save Contact</button>
                                                : ''
                                            }
                                            </Col>
                                        </Row>
                                        :''
                                    }
                                    {add ?
                                        <Row>
                                            <Col xs={6}>
                                                <input name={'nameContac'} onChange={this.handleChange}  type="text" placeholder="Name" required />
                                            </Col>
                                            <Col xs={6}>
                                                <input name={'emailContact'} onChange={this.handleChange} type="text" placeholder="Email" required />
                                            </Col>
                                        </Row>
                                        : ''
                                    }
                                </React.Fragment>
                            </div>                            
                        </div>
                    </Col>
                    <Col xs={12}>
                        {create == false ? <button className='delete' onClick={this.dalete}>Dalete</button> : ''}
                        {view === true? <button className='update' onClick={this.update}>Update</button> : '' }
                        {view === false && edit === true || create == true ?<button className='save' onClick={(e) => this.save(e)}>Save</button> : ''}
                    </Col>
                </Row>
            </div>
        )
    }
}
export default NewClient;